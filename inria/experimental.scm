;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2019, 2022 Inria

(define-module (inria experimental)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (cnrs irit)
  #:use-module (inria tainted storm)
  #:use-module (non-free cuda))

(define-public qr_mumps+cuda
  (package
   (inherit qr_mumps)
   (name "qr_mumps-cuda")
   (arguments
    (substitute-keyword-arguments (package-arguments qr_mumps)
                                  ((#:configure-flags flags '())
                                   `(cons "-DQRM_WITH_CUDA=ON" ,flags))))
   (inputs
    (modify-inputs (package-inputs qr_mumps)
      (prepend cuda)))
   (propagated-inputs
    (modify-inputs (package-propagated-inputs qr_mumps)
      (delete "starpu")
      (prepend starpu-1.3+cuda)))))
