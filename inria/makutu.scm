;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2020, 2024 Inria

;;; recipes of the makutu team
;;;
(define-module (inria makutu)
  #:use-module (guix)
  #:use-module (guix git) ;for 'git-checkout'
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module (guix-hpc-non-free packages solverstack)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (inria mpi)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cpio)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages commencement)
  #:use-module (guix-hpc packages solverstack)
  #:use-module (inria storm)
  #:use-module (guix-science-nonfree packages mkl)
  #:use-module (hacky mumps-mkl)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages fabric-management))

(define-public hou10ni
  (let ((commit "f12d26f704e6be0b17c2e144021eef4edba6dc1d")
        (revision "0"))
    (package
      (name "hou10ni")
      (version "0.5")
      (home-page "git@gitlab.inria.fr:hou10ni/hou10ni2d.git")

      ;; This will clone the repository over SSH on the client side, provided
      ;; you have permissions to do so.license:bsd-3
      (source
       (git-checkout (url "git@gitlab.inria.fr:hou10ni/hou10ni2d.git")
                     (commit commit)))

      (build-system cmake-build-system)
      (arguments
       `(#:phases (modify-phases %standard-phases
                    (add-after 'build 'mpi-setup
                      ;; Set the test environment for Open MPI.
                      ,%openmpi-setup))

         #:tests? #f
         #:build-type "Release")) ;FIXME
      (native-inputs `(("gfortran" ,gfortran)))
      (inputs `(("mumps-openmpi" ,mumps-openmpi)
                ("metis" ,metis)
                ("openblas" ,openblas)
                ("maphys" ,maphys)
                ("pastix" ,pastix)
                ("starpu" ,starpu)
                ("scalapack" ,scalapack)
                ("pt-scotch" ,pt-scotch)
                ("scotch" ,scotch)
                ("openmpi" ,openmpi)
                ("openssh" ,openssh)
                ("arb" ,arb)))
      (synopsis "Hou10ni")
      (description "hou10ni is a DG wave propagation simulating library ")
      (license #f))))

;;; recipes for the Ibrahima's thesis

(define-public block-row-mv
  (package
    (name "block-row-mv")
    (home-page
     "https://gitlab.inria.fr/solverstack/mini-examples/block-row-mv")
    (version "0.01")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit "31cbff45e871917b821702851bb8e5ccae21e0d9"))) ;uri
       (sha256
        (base32 "1hsn3y112i17x56qfxivllaysxlcgzl0b5kpg88a30c9fbhmmfzy"))))
    (build-system cmake-build-system)
    (arguments
     '(#:configure-flags '("-DUSE_COMPOSE:BOOL=OFF" "-DUSE_MKL:BOOL=OFF")))
    (synopsis "Mini-example from Ibrahima Djiba's thesis")
    (native-inputs (list gcc-toolchain gfortran-toolchain))
    (propagated-inputs (list openblas))
    (description
     "un super logiciel de simulation d'electromagnetique qui passe a l'echelle")
    (license #f) ;for now it is private
    ))

(define-public block-row-mv+compose
  (package
    (inherit block-row-mv)
    (name "block-row-mv-compose")
    (build-system cmake-build-system)
    (arguments
     '(#:configure-flags '("-DUSE_COMPOSE:BOOL=ON"
                           "-DTHREADS_PREFER_PTHREAD_FLAG=ON")))
    (inputs (modify-inputs (package-inputs block-row-mv+mkl)
              (append maphys++
                      pkg-config
                      blaspp
                      lapackpp
                      chameleon
                      mumps-openmpi
                      pastix
                      fabulous
                      arpack-ng)))))

(define-public block-row-mv+mkl
  (package
    (inherit block-row-mv)
    (name "block-row-mv-mkl")
    (build-system cmake-build-system)
    (arguments
     '(#:configure-flags '("-DUSE_COMPOSE:BOOL=OFF" "-DUSE_MKL:BOOL=ON")))
    (propagated-inputs (modify-inputs (package-propagated-inputs block-row-mv)
                         (replace "openblas" intel-oneapi-mkl)))))

(define-public block-row-mv+mkl+compose
  (package
    (inherit block-row-mv+mkl)
    (name "block-row-mv-mkl-compose")
    (build-system cmake-build-system)
    (arguments
     '(#:configure-flags '("-DUSE_COMPOSE:BOOL=ON" "-DUSE_MKL:BOOL=ON"
                           "-DTHREADS_PREFER_PTHREAD_FLAG=ON")))
    (inputs (modify-inputs (package-inputs block-row-mv+mkl)
              (append maphys++-mkl
                      pkg-config
                      blaspp-mkl
                      lapackpp-mkl
                      chameleon+mkl
                      mumps-mkl-openmpi
                      pastix-mkl-6
                      fabulous-mkl
                      arpack-ng)))))
