;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Note that this module provides packages that depend on "non-free"
;;; software, which denies users the ability to study and modify it.
;;;
;;; Copyright © 2023 Inria

(define-module (llnl tainted geos)
  #:use-module (guix)
  #:use-module (guix git-download)
  #:use-module (non-free parmetis)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system python)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages check)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages sphinx)
  #:use-module (guix-science-nonfree packages cuda)
  #:use-module (llnl geos))


(define-public camp-cuda
  (package/inherit camp
    (name "camp-cuda")
    (version "0.4.0")
    (build-system cmake-build-system)
    (arguments
    (list #:tests? #f; we need a cuda device to tests
         #:configure-flags #~`("-DENABLE_DOCS=OFF" 
                                "-DENABLE_CUDA:BOOL=ON"
                                "-DENABLE_FORTRAN=OFF"
                                "-DENABLE_TESTS=OFF"
                                "-DBLT_CXX_STD:STRING=c++17"
                                "-DCMAKE_CXX_FLAGS:STRING=-std=c++17"
                                "-DENABLE_MPI=ON"
                                ,(string-append "-DBLT_SOURCE_DIR="
                                                #$(this-package-input "blt")
                                                "/blt_dir")
                                "-DBUILD_SHARED_LIBS=ON"
                                "-DENABLE_OPENMP=ON")))
  (inputs (list cuda openmpi blt openssh-sans-x))))

(define-public raja-cuda
  (package/inherit raja
    (name "raja-cuda")
    (inputs (modify-inputs (package-inputs raja)
              (delete "camp")
              (append camp-cuda)
              (append cuda)
              ))
    (build-system cmake-build-system)
    (arguments
      (list
       #:tests? #f
       #:configure-flags #~`("-DENABLE_CUDA=ON" ;; FIXME : inherit this from raja recipe and delete the redundant flags
                              ,(string-append "-DCUDA_TOOLKIT_ROOT_DIR=" #$(this-package-input "cuda-toolkit"))
                              ,(string-append "-Dcamp_DIR=" #$(this-package-input "camp-cuda"))

							  "-DBLT_CXX_STD:STRING=c++17"
                              "-DCMAKE_CXX_FLAGS:STRING=-std=c++17"
                              "-DENABLE_MPI=ON"
                              ,(string-append "-DBLT_SOURCE_DIR="
                                               #$(this-package-input "blt")
                                               "/blt_dir")
                              "-DENABLE_TESTS=OFF"
                              "-DENABLE_BENCHMARKS=OFF"
                              "-DENABLE_GTEST=OFF"
                              "-DRAJA_ENABLE_EXERCISES=OFF"
                              "-DBUILD_SHARED_LIBS=ON"
                              "-DRAJA_ENABLE_CUDA=ON" 
                              "-DENABLE_OPENMP=ON"
                              "-DRAJA_ENABLE_OPENMP=ON"
                              "-DCMAKE_CUDA_ARCHITECTURES=70")))))

(define-public chai-cuda
  (package/inherit chai
    (name "chai-cuda")
    (inputs (modify-inputs (package-inputs chai)
              (delete "camp")
              (delete "raja")
              (append camp-cuda)
              (append raja-cuda)
              (append cuda)))
    (arguments
     (list
       #:tests? #f
       #:configure-flags  ;; FIXME : same than before : use substitute-keyword-arguments and delete redundant flags
           #~`("-DENABLE_CUDA=ON"
              "-DCHAI_ENABLE_TESTS=OFF"
              "-DENABLE_TESTS=OFF"
              ,(string-append "-DBLT_SOURCE_DIR="
                   #$(this-package-input "blt")
                   "/blt_dir")
              "-DCHAI_ENABLE_BENCHMARKS=OFF"
			  ,(string-append "-Dcamp_DIR=" #$(this-package-input "camp-cuda"))
              "-DCHAI_ENABLE_EXAMPLES=OFF"
              "-DCMAKE_CUDA_STANDARD=14"
              "-DBUILD_SHARED_LIBS=ON"
              "-DENABLE_EXAMPLES:BOOL=OFF"
			  "-DRAJA_ENABLE_EXERCISES=OFF"
			  "-DCUDA_ARCH=sm_70"
              "-DENABLE_GTEST_DEATH_TESTS=OFF"
              "-Dgtest_disable_pthreads=OFF"
              "-DENABLE_OPENMP=ON" 
              "-DENABLE_GMOCK=OFF"
              "-DENABLE_GTEST=ON"
              "-DUMPIRE_ENABLE_C=ON"
              "-DCMAKE_CUDA_ARCHITECTURES=70")
       #:phases
              #~(modify-phases %standard-phases
                (add-after 'unpack 'copy-umpire-sources
                   (lambda _
                     (begin
                       (rmdir "src/tpl/umpire")
                       (copy-recursively (string-append #$(this-package-input
                                          "umpire") "/umpire_dir") "src/tpl/umpire"))))) 

)))) 

(define-public caliper-cuda
  (package/inherit caliper
    (name "caliper-cuda")
    (inputs (modify-inputs (package-inputs caliper)
              (append cuda)))
    (arguments
     (substitute-keyword-arguments (package-arguments caliper)
       ((#:configure-flags flags #~'())
        #~(append '("-DENABLE_CUDA=ON"
                    "-DCMAKE_CUDA_ARCHITECTURES=70") ;XXX: adjust?
                  #$flags))
       ((#:tests? _ #t)
        ;; Builds won't necessarily happen on a machine with CUDA GPUs so
        ;; don't try to run tests.
        #f)))))

(define-public hypre-geos
  (let ((version "2.29.0")
        (revision "1"))
    (package
      (inherit hypre) ; FIXME : we must find a way to avoid compil doc because it is slow and useless
      (name "hypre-geos")
      (version "2.29.0")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/hypre-space/hypre")
                      (commit (string-append "v" version))))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0d60dxhf4h3sfpzsigbjdc9vy2sdimq9vxidgvilyj7gckkji761"))))
      (arguments
       (substitute-keyword-arguments (package-arguments hypre)
         ((#:configure-flags _ #~'())
          #~`("--enable-shared"
              "--with-dsuperlu" "--with-dsuperlu-lib=-lsuperlu_dist"
              "--with-openmp" "--enable-mixedint"))
         ((#:phases phases #~%standard-phases)
          ;;XXX: Tests appear to fail and #:tests? is not honored.
          #~(modify-phases #$phases
              (delete 'check)))))
      (native-inputs (modify-inputs (package-native-inputs hypre)
                       ;; hypre@2.2.0 doesn't need this input; add it here.
                       (append python-sphinx-rtd-theme)))
      (inputs (list openblas
                    lapack
                    gfortran
                    chai
                    superlu-dist-64
                    parmetis-i64
                    metis4parmetis-i64)))))

(define-public hypre-geos-cuda
  (package/inherit hypre-geos
    (name "hypre-geos-cuda")
    (inputs (modify-inputs (package-inputs hypre-geos)
              (delete "chai")
              (append cuda chai-cuda)))
    (arguments
     (substitute-keyword-arguments (package-arguments hypre-geos)
       ((#:configure-flags flags #~'())
        #~(append `("--with-cuda"
                    "--enable-cusparse"
                    "--enable-unified-memory"
                    ,(string-append "--with-cuda-home="
                                    #$(this-package-input "cuda-toolkit"))
                    "--with-gpu-arch=70"
                    "--with-umpire"
                    ,(string-append "--with-umpire-include="
                                    #$(this-package-input "chai-cuda")
                                    "/include")
                    ,(string-append "--with-umpire-lib-dirs="
                                    #$(this-package-input "chai-cuda")
                                    "/lib")
                    "--with-umpire-libs=umpire")
                  #$flags))
       ((#:tests? _ #t)
        ;; Builds won't necessarily happen on a machine with CUDA GPUs so
        ;; don't try to run tests.
        #f)))))


(define-public suitesparse-geos
  (package/inherit suitesparse
    (name "suitesparse-geos")
    (version "5.10.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/drtimothyaldendavis/suitesparse")
                    (commit (string-append "v" version))))
              (patches (search-patches "suitesparse-mongoose-cmake.patch"))
              (sha256 (base32
                       "19gx5wlgqnqpgz6mvam9lalyzpbfwgqhppps8z3np9sh0mgaiyw9"))
              (modules '((guix build utils)))
              (snippet '(begin
                          (delete-file-recursively "metis-5.1.0") #t))))

    (inputs (modify-inputs (package-inputs suitesparse)
              (delete "metis")
              (append metis4parmetis-i64)))

    (arguments (substitute-keyword-arguments (package-arguments suitesparse)
                 ((#:phases phases
                   '%standard-phases)
                  `(modify-phases ,phases
                     (add-after 'unpack 'fix-cc-long
                       (lambda _
                         (begin
                           (setenv "CFLAGS"
                            (string-append "-D'SuiteSparse_long=long long int' "
                                           "-DSuiteSparse_long_max=2147483647 "
                                           "-DSuiteSparse_long_idd=\"d\" "
                                           "-D'SuiteSparse_long_id=\"%d\"'")))))
                     (replace 'install
                       (lambda _
                         (for-each (lambda (directory)
                                     (invoke "make"
                                             "-C"
                                             directory
                                             (string-append "CC="
                                                            ,(cc-for-target))
                                             "BLAS=-lopenblas"
                                             "LAPACK=-lopenblas"
                                             "MY_METIS_LIB=-lmetis"
                                             "install" ; target install
                                             (format #f "-j~a"
                                                     (parallel-job-count))))
                                   (list "SuiteSparse_config"
                                         "AMD"
                                         "BTF"
                                         "CAMD"
                                         "CCOLAMD"
                                         "COLAMD"
                                         "CHOLMOD"
                                         "KLU"
                                         "UMFPACK"))))
                     (replace 'build
                       (lambda _
                         (for-each (lambda (directory)
                                     (invoke "make"
                                             "-C"
                                             directory
                                             (string-append "INSTALL_LIB="
                                                            (assoc-ref
                                                             %outputs "out")
                                                            "/lib")
                                             (string-append "INSTALL_INCLUDE="
                                              (assoc-ref %outputs "out")
                                              "/include")
                                             (string-append "CC="
                                                            ,(cc-for-target))
                                             "BLAS=-lopenblas"
                                             "LAPACK=-lopenblas"
                                             "MY_METIS_LIB=-lmetis"
                                             "library" ;just compile library
                                             (format #f "-j~a"
                                                     (parallel-job-count))))
                                   (list "SuiteSparse_config"
                                         "AMD"
                                         "BTF"
                                         "CAMD"
                                         "CCOLAMD"
                                         "COLAMD"
                                         "CHOLMOD"
                                         "KLU"
                                         "UMFPACK"))))))))))

(define (geosx-configure-flags package)
  #~`("-DENABLE_DOCS=OFF" "-DENABLE_EXAMPLES=OFF"
      "-DENABLE_FORTRAN=OFF"
      "-DENABLE_TESTS=ON"
      "-DENABLE_PVTPackage=ON"
      "-DENABLE_DOXYGEN=OFF"
      "-DENABLE_MPI=ON"
      "-DENABLE_PYGEOSX=ON"
      ,(string-append "-DBLT_SOURCE_DIR="
                      #$(lookup-package-input package "blt")
                      "/blt_dir")
      ,(string-append "-Dadiak_DIR="
                      #$(lookup-package-input package "adiak"))
      ,(string-append "-DCALIPER_DIR="
                      #$(lookup-package-input package "caliper"))
      ,(string-append "-DCONDUIT_DIR="
                      #$(lookup-package-input package "conduit"))
      ,(string-append "-DSILO_DIR="
                      #$(lookup-package-input package "silo"))
      ,(string-append "-DPUGIXML_DIR="
                      #$(lookup-package-input package
                                              "pugixml-geos"))
      ,(string-append "-DHYPRE_DIR="
                      #$(lookup-package-input package
                                              "hypre-geos"))
      ,(string-append "-DSUPERLU_DIST_DIR="
                      #$(lookup-package-input package
                                              "superlu-dist-64"))
      ,(string-append "-DMETIS_DIR="
                      #$(lookup-package-input package
                                              "metis4parmetis-i64"))
      ,(string-append "-DPARMETIS_DIR="
                      #$(lookup-package-input package
                                              "parmetis-i64"))
      ,(string-append "-DRAJA_DIR="
                      #$(lookup-package-input package
                                              "raja"))
      ,(string-append "-DCHAI_DIR="
                      #$(lookup-package-input package
                                              "chai"))
      ,(string-append "-DUMPIRE_DIR="
                      #$(lookup-package-input package
                                              "chai") "/lib/cmake")
      ,(string-append "-DFMT_DIR="
                      #$(lookup-package-input package "fmt"))
      ,(string-append "-DSUITESPARSE_DIR="
                      #$(lookup-package-input package
                                              "suitesparse-geos"))
      ,(string-append "-DVTK_DIR="
                      #$(lookup-package-input package
                                              "vtk-geos"))
      "-DBLT_CXX_STD:STRING=c++17"
      "-DENABLE_XML_UPDATES=OFF"
      "-DENABLE_UNCRUSTIFY=OFF"
      "-DENABLE_PVTPackage=ON"
      "-DENABLE_MATHPRESSO=OFF"
      "-DGEOSX_BUILD_SHARED_LIBS=OFF"
      "-DENABLE_VTK=ON"
      "-DENABLE_TRILINOS=OFF"
      "-DENABLE_OPENMP=ON"
      "-DENABLE_GTEST=ON"
      "-DENABLE_PETSC=OFF"
      "-DENABLE_SCOTCH=OFF"
      "-DCMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING=-g0 -O2 -DNDEBUG"
      "-DENABLE_SUITESPARSE=ON"
      "-DENABLE_BENCHMARKS=OFF"
      ,(string-append "-DHDF5_DIR="
                      #$(lookup-package-input package "hdf5-geos"))))

(define-public geosx
  (let ((commit "5b20fd397c152fadc4e7f12b6209580221295634")
        (revision "1"))
    (package
      (name "geosx")
      (version (git-version "0.2.0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/GEOS-DEV/GEOS")
                      (commit commit)))
                (file-name (git-file-name name commit))
                (sha256
                 (base32
                  "1bj2fxs4ibp2spk88fzmk5h950zq563yc6p94mqpk1mrbm7cfzy3"))))
      (build-system cmake-build-system)
      (inputs (list adiak
                    blt
                    caliper
                    camp
                    chai
                    conduit
                    fmt
                    googletest
                    hdf5-geos
                    hdf5-interface
                    hypre-geos
                    lapack
                    metis4parmetis-i64
                    openblas
                    openmpi
                    pvtpackage
                    lvarray
                    parmetis-i64
                    pugixml-geos
                    python
                    python-virtualenv
                    python-setuptools
                    python-h5py-geos
                    python-mpi4py-geos
                    python-numpy
                    python-scipy
                    raja
                    silo
                    suitesparse-geos
                    superlu-dist-64
                    vtk-geos))
      (arguments
       (list #:configure-flags (geosx-configure-flags this-package)
             #:phases #~(modify-phases %standard-phases
                          (add-before 'configure 'chdir-src-and-link
                            (lambda _
                              (begin
                                (chdir "src")
                                (delete-file-recursively "cmake/blt")
                                (symlink (string-append #$(this-package-input
                                                           "blt") "/blt_dir")
                                         "cmake/blt")
                                (delete-file-recursively "coreComponents/LvArray")
                                (copy-recursively (string-append #$(this-package-input
                                                                    "lvarray")
                                                                 "/lvarray")
                                                  "coreComponents/LvArray")
                                (delete-file-recursively
                                 "coreComponents/fileIO/coupling/hdf5_interface")
                                (copy-recursively (string-append #$(this-package-input
                                                                    "hdf5-interface")
                                                   "/hdf5_interface")
                                 "coreComponents/fileIO/coupling/hdf5_interface")
                                (delete-file-recursively
                                 "coreComponents/constitutive/PVTPackage")
                                (copy-recursively (string-append #$(this-package-input
                                                                    "pvtpackage")
                                                   "/pvtpackage")
                                 "coreComponents/constitutive/PVTPackage")))))))
      (home-page "https://github.com/GEOS-DEV/GEOS")
      (synopsis "Geophysical Simulation Framework")
      (description
       "GEOS is a simulation framework for modeling coupled flow,
     transport, and geomechanics in the subsurface.  The code provides
     advanced solvers for a number of target applications, including")
      (license license:lgpl2.1))))

(define-public geosx-cuda
  (package
    (inherit geosx)
    (source (origin
              (inherit (package-source geosx))
              (patches (list (local-file
                              "geosx-cuda-suppress-generate-xsd.patch"))))) 
    (name "geosx-cuda")
    (arguments
     (substitute-keyword-arguments (package-arguments geosx)
       ((#:tests? _ #f)
        #f)                            ;no driver on build machine → no test!
       ((#:configure-flags flags #~'())
        #~(append `("-DENABLE_CUDA=ON"
                    "-DCUDA_ARCH=sm_70"
                    ,(string-append "-DCUDA_TOOLKIT_ROOT_DIR="
                                    #$(this-package-input
                                       "cuda-toolkit"))
                    "-DRAJA_ENABLE_CUDA=ON"
                    "-DCMAKE_CUDA_ARCHITECTURES=70"
                    "-DCMAKE_CUDA_FLAGS=--std=c++17 --expt-relaxed-constexpr --expt-extended-lambda")
                  #$(geosx-configure-flags this-package)))))
    (inputs (modify-inputs (package-inputs geosx)
              (replace "camp" camp-cuda)
              (replace "chai" chai-cuda)
              (replace "raja" raja-cuda)
              (replace "caliper" caliper-cuda)
              (replace "camp" camp-cuda)
              (append cuda)))))
