;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; However, note that this module provides packages for "non-free" software,
;;; which denies users the ability to study and modify it.  These packages
;;; are detrimental to user freedom and to proper scientific review and
;;; experimentation.  As such, we kindly invite you not to share it.
;;;
;;; Copyright © 2018, 2019, 2020, 2022 Inria

(define-module (non-free cuda)
  ;; This module is kept for backward-compatibility.
  ;; Use (guix-science-nonfree packages cuda) instead.
  #:use-module (guix-science-nonfree packages cuda)
  #:re-export (cuda-8.0
               cuda-10.2
               cuda-11.0
               cuda-11.7)
  #:export (cuda))

(define-public cuda
  ;; Default version (possibly different from that in guix-science-nonfree).
  ;;
  ;; Note: Pick a version that matches the actual "driver"--i.e.,
  ;; /usr/lib64/libcuda.so available on the target machine.
  cuda-11.7)
