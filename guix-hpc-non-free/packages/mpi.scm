;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Note that this module provides packages that depend on "non-free"
;;; software, which denies users the ability to study and modify it.
;;;
;;; Copyright © 2024 Inria

(define-module (guix-hpc-non-free packages mpi)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (gnu packages mpi)
  #:use-module (guix-science-nonfree packages cuda))

(define-public hwloc-cuda
  (package/inherit hwloc
    (name (string-append (package-name hwloc) "-cuda"))
    (arguments
     (substitute-keyword-arguments (package-arguments hwloc)
       ((#:configure-flags flags)
        #~(append (list "--enable-cuda"
                        (string-append "--with-cuda=" #$(this-package-input "cuda-toolkit")))
                  #$flags))))
    (inputs (modify-inputs (package-inputs hwloc)
              (append cuda)))))
